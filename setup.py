from distutils.core import setup

setup(
	name='pyfinder',
	description='Database Virtual Filesystem',
	version='0.1',
	author='Leandro Resende Mattioli',
	author_email='leandro.mattioli@gmail.com',
	url='http://www.leandromattioli.com/pyfinder/',
	scripts=['runpyfinder'],
	packages=['pyfinder'],
	package_data = {
		'pyfinder': ['data/*']
	}
)
