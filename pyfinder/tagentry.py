from gi.repository import Gtk
from pyfinder.observable import Observable

#@TODO: Herdar GtkBox
#       Prover sinal 'clicked' sem precisar expor o botão de acesso

class TagEntry(Observable):
    def __init__(self):
        """Initializer"""
        Observable.__init__(self)
        self._tags = []
        self._createWidgets()
        self._configEvents()
        
    def _createWidgets(self):
        """Creates base UI widgets"""
        self.box = Gtk.Box(Gtk.Orientation.HORIZONTAL, 3)
        self.terms = Gtk.Entry()
        self.terms.set_placeholder_text('Entre com as tags')
        self.button = Gtk.Button(stock=Gtk.STOCK_FIND)
        self.completion = Gtk.EntryCompletion()
        self.box.pack_start(self.terms, True, True, 3)
        self.box.pack_start(self.button, False, True, 3)
        self.button.set_can_default(True)
        
    def _configEvents(self):   
        #@TODO: Retrieve tags...
        self.terms.set_completion(self.completion)
        self._liststore = Gtk.ListStore(str)
        self.completion.set_model(self._liststore)
        self.completion.set_text_column(0)
        self.completion.connect('match-selected', self._addTag)
        self.terms.connect('backspace', self._backspace)
        
    # Accessors
    
    def getContainer(self):
        return self.box
        
    def getEntry(self):
        return self.terms
        
    def getActionButton(self):
        return self.button
        
    def getTags(self):
        return self._tags
        
    def setTagList(self, tags):
        self._liststore.clear()
        for match in tags:
            self._liststore.append([match])
    
    #Callbacks
    
    def _addTag(self, widget, liststore, treeiter):
        """Adds a tag to the tag list and to the view"""
        #Getting tag
        tag = liststore.get_value(treeiter, 0)
        currText = self.terms.get_text()
        self.terms.set_text('')
        self._tags.append(tag)
        #Add button
        button = Gtk.Button(label=tag)
        img = Gtk.Image.new_from_stock(Gtk.STOCK_CLOSE, 
                                       Gtk.IconSize.SMALL_TOOLBAR)
        button.set_relief(Gtk.ReliefStyle.NONE)
        button.set_image(img)
        button.set_image_position(Gtk.PositionType.RIGHT)
        button.connect('clicked', self._removeTag)
        self.box.pack_start(button, False, False, 3)
        pos = len(self._tags) - 1
        self.box.reorder_child(button, pos)
        button.show()
        self.notifyAll('tag-added')
        return True
    
    def _removeTag(self, widget, *data): 
        """Remove a tag from the tag list and from the view"""
        self._tags.remove(widget.get_label())
        self.box.remove(widget)
        self.terms.grab_focus()
        self.notifyAll('tag-removed')
        
    def _backspace(self, widget, *data):
        """Handles backspace event: delete tag if no character left"""
        if not widget.get_text():
            children = self.box.get_children()
            if not len(children):
                return
            lastTag = children[len(self._tags) - 1]
            self._removeTag(lastTag, None)
            
if __name__ == "__main__":
    win = Gtk.Window()
    tagEntry = TagEntry()
    win.add(tagEntry.box)
    tagEntry.button.grab_default()
    tagEntry.setTagList(['lala', 'teste', 'spam'])
    win.show_all()
    win.connect("delete-event", Gtk.main_quit)
    Gtk.main()
