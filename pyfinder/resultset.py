from time import time
from os import path, stat
from enum import Enum
from pyfinder.thumbnailerclient import ThumbnailerClient
from pyfinder.uriutils import uri2path

class OrderCriteria(Enum):
    atime = 1
    mtime = 2
    ctime = 3
    size = 4
    name = 5

class ResultSet:

    def __init__(self):
        self._entries = []
        self._order = OrderCriteria.name
        self._reverse = False
        self._thumbnailer = ThumbnailerClient()
    
    def getResultSet(self):
        return self._entries
        
    def setReverse(self, reverse):
        self._reverse = reverse
        
    def setOrder(self, order):
        self._order = order
       
    ##################################################################   
    # Result Set
    ################################################################## 
        
    def setFromURIList(self, uris):
        self._entries = [ {'uri': x} for x in uris ]
        for e in self._entries:
            e['path'] = uri2path(e['uri'])
            try:
                attrs = stat(e['path'])
                e['mtime'] = attrs.st_mtime
                e['ctime'] = attrs.st_ctime
                e['atime'] = attrs.st_atime
                e['size'] = attrs.st_size
                e['baseName'] = path.basename(e['path'])
                e['thumb'] = self._thumbnailer.makeThumbnail(e['path'])
                e['invalid'] = False
            except FileNotFoundError:
                e['invalid'] = True
        self._entries = [e for e in self._entries if not e['invalid']]
        
        if self._order == OrderCriteria.atime:
            self.orderByDate('atime')
        elif self._order == OrderCriteria.mtime:
            self.orderByDate('mtime')
        elif self._order == OrderCriteria.ctime:
            self.orderByDate('ctime')
        elif self._order == OrderCriteria.size:
            self.orderBySize()
        else:
            self.orderByName()

    ##################################################################   
    # Order Functions
    ##################################################################
        
    def orderByDate(self, dateKey):
        epoch_time = int(time())
        ascend = lambda x: int(round(x))
        descend = lambda x: epoch_time - int(round(x))
        fn = descend if self._reverse else ascend
        for e in self._entries:
            e['value'] = fn(e[dateKey])
    
    def orderBySize(self):
        ascend = lambda x: x
        descend = lambda x: (1/(x+1)) * 1e6
        fn = descend if self._reverse else ascend
        for e in self._entries:
            e['value'] = fn(e['size'])
        
    def orderByName(self):
        #Alphabetic order already means reverse!
        reverse = not self._reverse
        k = lambda x: x['baseName']
        self._entries = sorted(self._entries, key=k, reverse=reverse)
        value = 5
        for e in self._entries:
            e['value'] = value
            value += 1
