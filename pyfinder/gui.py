#!/usr/bin/env python3

from os import path, remove, rename
from subprocess import Popen
from gi.repository import Gtk, Gio, Gdk, Keybinder
from pyfinder.config import *
from pyfinder.tagentry import TagEntry
from pyfinder.trackerclient import TrackerClient
from pyfinder.inspector import Inspector
from pyfinder.sidebar import Sidebar, ResultViewType
from pyfinder.resultset import ResultSet
from pyfinder.treemapview import TreemapView
from pyfinder.iconresultview import IconResultView
from pyfinder.detailsresultview import DetailsResultView
from pyfinder.scrolledstack import ScrolledStack

#ResultSet: list of dictionaries (one dict per file)

class GUI:
    def __init__(self):
        self._tracker = TrackerClient()
        self._resultSet = ResultSet()
        self._loadWidgets()
        self._inspector = Inspector(self._builder)
        self._sidebar = Sidebar(self._builder)
        self._sidebar.subscribe(self)
        self._addTagEntry()
        self._addResultView()
        self._sidebar.setDefaultState()
        self._configGlobalKeybinding()
        self._configEvents()
    
    ##################################################################
    # General GUI config
    ##################################################################
    
    def _loadWidgets(self):
        self._builder = Gtk.Builder()
        self._builder.add_from_file(path.join(DATA_PATH, UI_FILE))
        self._builder.connect_signals(self)
        # General Widgets
        self.win = self._builder.get_object('window1')
        self._terms = self._builder.get_object('entryTerms')
        self.box = self._builder.get_object('mainBox')
        self.completion = self._builder.get_object('completion')
        self.statusIcon = self._builder.get_object('statusIcon')
        self._menu = self._builder.get_object('popup')
        self._stackContainer = self._builder.get_object('stackContainer')
        # Dialogs
        self.diagAbout = self._builder.get_object('dialogAbout')
        self.diagRename = self._builder.get_object('dialogRename')
        self.entryRename = self._builder.get_object('entryRename')

    def _configEvents(self):
        self.win.connect('key-press-event', self.keypress)
    
    def _configGlobalKeybinding(self):
        #Global Keybinding
        Keybinder.init()
        keystr = "<Super>n"
        Keybinder.bind(keystr, self.toggleWindowVisibility, None)
        
    def popup(self, widget, button, time, data = None):
        if button == 3:
            self._menu.show_all()
            self._menu.popup(
                parent_menu_shell=None,
                parent_menu_item=None,
                data=self.statusIcon,
                func=Gtk.StatusIcon.position_menu, 
                button=3, 
                activate_time=time
            )
    
    def toggleWindowVisibility(self, sender, *userdata):
        if self.win.get_visible():
            self.win.hide()
        else:
            self.win.show_all()
            self._tagEntry.terms.grab_focus()

    ##################################################################
    # About Dialog
    ##################################################################

    def showAbout(self, sender, *userdata):
        self.diagAbout.run()
    
    def hideAbout(self, sender, *userdata):
        self.diagAbout.hide()
        
    ##################################################################
    # Main Window
    ##################################################################
        
    def show(self):
        self.win.show_all()
        self.win.maximize()
        self.showResults(None)
        #Gtk.main()
      
    def quit(self, *args):
        self.win.emit('delete-event', None)
        
    def keypress(self, widget, event):
        if event.keyval == Gdk.KEY_F9:
            self._sidebar.toggle()
    
    ##################################################################
    # Application Specific
    ##################################################################
        
    def _addTagEntry(self):
        self._tagEntry = TagEntry()
        container = self._tagEntry.getContainer()
        self.box.pack_start(container, False, True, 3)
        self.box.reorder_child(container, 0)
        container.show_all()
        self._tagEntry.setTagList(self._tracker.getAllTags())
        self.actionBtn = self._tagEntry.getActionButton()
        self.actionBtn.connect('clicked', self.showResults)
        self._tagEntry.subscribe(self)
        
    def _addResultView(self):
        self._stack = ScrolledStack()
        self._stack.set_homogeneous(False)
        self._stack.set_transition_type (Gtk.StackTransitionType.CROSSFADE)
        self._stack.set_transition_duration(500)
        self._stackContainer.pack_start(self._stack, True, True, 0)
        self._iconView = IconResultView()
        self._iconView.subscribe(self)
        self._iconView.show()
        self._treemap = TreemapView()
        self._treemap.subscribe(self)
        self._treemap.show()
        self._detailsView = DetailsResultView()
        self._detailsView.subscribe(self)
        self._detailsView.show()
        self._stack.add_named(self._iconView, ResultViewType.iconview.name)
        self._stack.add_named(self._treemap, ResultViewType.treemap.name)
        self._stack.add_named(self._detailsView, ResultViewType.details.name,
                              hpolicy=Gtk.PolicyType.AUTOMATIC)
        self._stack.set_visible_child_name(ResultViewType.iconview.name)
        self._resultView = self._stack.get_visible_child()

    def showResults(self, widget=None, data=None):
        fileList = self._tracker.getFileList(self._tagEntry.getTags())
        limit = self._sidebar.getLimit()
        if limit:
            fileList = fileList[0:limit]
        self._resultSet.setReverse(self._sidebar.getReverse())
        self._resultSet.setOrder(self._sidebar.getOrderCriteria())
        self._resultSet.setFromURIList(fileList)
        self._resultView.view(self._resultSet.getResultSet())
        #rect = self._stackContainer.get_allocation()
        #self._iconView.set_size_request(rect.width, -1)

    ##################################################################
    # File Actions
    ##################################################################
    
    def openFile(self, widget, *data):
        pass

    def renameFile(self, widget, *data):
        #@TODO: update database!
        #@TODO: Block invalid characters
        entry = self._inspector.getSelectedEntry()
        baseName = entry['baseName']
        filepath = entry['path']
        self.entryRename.set_text(baseName)
        last = len(baseName) - 4
        self.entryRename.select_region(0, last)
        response = self.diagRename.run()
        self.diagRename.hide()
        if response == Gtk.ResponseType.OK:
            newName = self.entryRename.get_text()
            print(newName)
            if not newName:
                return
            #rename(filepath, filepath.replace(baseName, newName))
        self.showResults()
        
        
    def deleteFile(self, widget, *data):
        #@TODO: update database!
        entry = self._inspector.getSelectedEntry()
        diag = Gtk.MessageDialog(self.win, Gtk.DialogFlags.MODAL,
                          Gtk.MessageType.QUESTION,
                          Gtk.ButtonsType.YES_NO,
                          'Tem certeza que deseja excluir: \n' +
                          entry['baseName'])
        response = diag.run()
        diag.hide()
        if response == Gtk.ResponseType.YES:
            remove(entry['path'])
        #@OPTIMIZE: only remove from liststore if iconview or detailsview
        self.showResults()
      
    ##################################################################    
    # Notifications
    ##################################################################
    
    def notify(self, source, event, data):
        if source == self._sidebar:
            self._handleSidebarEvent(event, data)
            self.showResults()
        elif source == self._tagEntry:
            self.showResults()
        elif event=='file-details':
            self._inspector.update(data)
        elif event=='file-clicked':
            Popen(['xdg-open', data])
            #self.toggleWindowVisibility(None)
        elif event == 'selection-cleared':
            self._inspector.update(None)

    def _handleSidebarEvent(self, event, data):
        if event == 'resultview-changed':
            resultViewType = self._sidebar.getResultViewType()
            self._stack.set_visible_child_name(resultViewType.name) 
            self._resultView = self._stack.get_visible_child()

def main():
    t = GUI()
    t.show()
    
if __name__ == '__main__':
    main()
