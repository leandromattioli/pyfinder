from gi.repository import Gtk

class ScrolledStack(Gtk.Stack):
    def __init__(self):
        Gtk.Stack.__init__(self)
        self.widgets = {}
        
    def add_named(self, widget, name, hpolicy=Gtk.PolicyType.NEVER,
                                      vpolicy=Gtk.PolicyType.AUTOMATIC):
        win = Gtk.ScrolledWindow()
        win.set_policy(hpolicy, vpolicy)
        win.add(widget)
        win.show_all()
        Gtk.Stack.add_named(self, win, name)
        self.widgets[name] = widget
        
    def get_visible_child(self):
        win = Gtk.Stack.get_visible_child(self)
        return win.get_child()
        
    def get_visible_scrolledwindow(self):
        return Gtk.Stack.get_visible_child(self)
    
