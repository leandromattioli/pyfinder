from enum import Enum
from pyfinder.observable import Observable
from pyfinder.resultset import OrderCriteria

class ResultViewType(Enum):
    iconview = 1
    treemap = 2
    details = 3

class Sidebar(Observable):
    def __init__(self, builder):
        Observable.__init__(self)
        self.builder = builder
        self._loadWidgets()
        self._configEvents()
        
    def setDefaultState(self):
        """Sets the sidebar to its default state"""
        self.radioIcon.set_active(True)
        
    def toggle(self):
        """Toggles sidebar visibility"""
        self.expander.set_expanded(not self.expander.get_expanded())
        
    def _loadWidgets(self):
        """Loads relevant widgets from GUI builder"""
        self.expander = self.builder.get_object('expanderSidebar')
        # Search Parameters
        self.checkLimit = self.builder.get_object('checkLimit')
        self.scaleLimit = self.builder.get_object('scaleLimit')
        #Order criteria
        self.checkReverse = self.builder.get_object('checkReverseOrder')
        self.radioATime = self.builder.get_object('radioATime')
        self.radioMTime = self.builder.get_object('radioMTime')
        self.radioCTime = self.builder.get_object('radioCTime')
        self.radioSize = self.builder.get_object('radioSize')
        self.radioName = self.builder.get_object('radioName')
        self.orderWidgets = [
            self.checkReverse, self.radioSize, self.radioName,
            self.radioATime, self.radioMTime, self.radioCTime,
        ]
        #ResultViews
        self.radioIcon = self.builder.get_object('radioIcon')
        self.radioTreemap = self.builder.get_object('radioTreemap')
        
    def _configEvents(self):
        """Configures widgets events"""
        #ResultView
        self.radioIcon.connect('toggled', self._toggleResultView)
        self.radioTreemap.connect('toggled', self._toggleResultView)
        #OrderCriteria
        self.checkReverse.connect('toggled', self._toggleOrderCriteria)
        self.radioATime.connect('toggled', self._toggleOrderCriteria)
        self.radioMTime.connect('toggled', self._toggleOrderCriteria)
        self.radioCTime.connect('toggled', self._toggleOrderCriteria)
        self.radioSize.connect('toggled', self._toggleOrderCriteria)
        self.radioName.connect('toggled', self._toggleOrderCriteria)
        #Limit
        self.checkLimit.connect('toggled', self._toggleLimit)
        self.scaleLimit.connect('value-changed', self._toggleLimit)
        
    ##################################################################
    # Event handling and notifications
    ##################################################################
    
    def _setIgnoreSidebarOrdering(self, boolean):
        for i in self.orderWidgets:
            i.set_sensitive(not boolean)
        
    def _toggleResultView(self, widget, *data):
        """Toggles result view type and fires change event"""
        resultViewType = self.getResultViewType()
        ignore = (resultViewType == ResultViewType.details)
        self._setIgnoreSidebarOrdering(ignore)
        self.notifyAll('resultview-changed')
        
    def _toggleOrderCriteria(self, widget, *data):
        """Toggles order criteria and fires change event"""
        self.notifyAll('ordercriteria-changed')
        return True
        
    def _toggleLimit(self, widget, *data):
        """Toggles limit result state and fires change event"""
        active = self.checkLimit.get_active()
        self.scaleLimit.set_sensitive(active)
        self.notifyAll('limit-changed')
     
    ##################################################################
    # Read-only accessors
    ##################################################################
        
    def getReverse(self):
        """Checks whether the 'reverse order' option is enabled"""
        return self.checkReverse.get_active()
        
    def getResultViewType(self):
        """Returns the selected ResultView type"""
        if self.radioIcon.get_active():
            return ResultViewType.iconview
        elif self.radioTreemap.get_active():
            return ResultViewType.treemap
        else:
            return ResultViewType.details
        
    def getOrderCriteria(self):
        """Gets the current order criteria"""
        if self.radioATime.get_active():
            return OrderCriteria.atime
        elif self.radioMTime.get_active():
            return OrderCriteria.mtime
        elif self.radioCTime.get_active():
            return OrderCriteria.ctime
        elif self.radioSize.get_active():
            return OrderCriteria.size
        else:
            return OrderCriteria.name  
            
    def getLimit(self):
        """Returns result limit value or False for no limits"""
        if self.checkLimit.get_active():
            return int(self.scaleLimit.get_value())
        return False
        
