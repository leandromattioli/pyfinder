from time import gmtime, strftime
from os.path import expanduser
from pyfinder.observable import Observable
from pyfinder.config import *

LBLPATH_FMT = '<a href="file://%s">%s</a>'

class Inspector:
    def __init__(self, builder):
        self.builder = builder
        self.lblName = self.builder.get_object('lblName')
        self.lblSize = self.builder.get_object('lblSize')
        self.lblMTime = self.builder.get_object('lblMTime')
        self.lblCTime = self.builder.get_object('lblCTime')
        self.lblATime = self.builder.get_object('lblATime')
        self.lblPath = self.builder.get_object('lblPath')
        self.comboSizeUnits = self.builder.get_object('comboSizeUnits')
        self.comboSizeUnits.set_active(1) #defaults: kBytes
        self.comboSizeUnits.connect('changed', self._showSize)
        self.btnRename = self.builder.get_object('btnRename')
        self.btnDelete = self.builder.get_object('btnDelete')
        self.btnRename.set_sensitive(False)
        self.btnDelete.set_sensitive(False)
        
    def _fmtTime(self, epoch):
        """Formats time according to locale preferences"""
        #@TODO Get the formatting string from preferences
        t = gmtime(epoch)
        return strftime("%c (%x)", t)
        
    def _showSize(self, widget=None, *data):
        """Shows the human readable file size"""
        if not self.entry:
            return
        k = self.comboSizeUnits.get_active() * 10
        if k == 0:
            lblSize = str(self.entry['size'])
        else:
            unit = 2 ** k
            size = self.entry['size'] / unit
            lblSize = "%.3f" % size
        self.lblSize.set_text(lblSize)
        
    def getSelectedEntry(self):
        return self.entry
    
    def update(self, entry):
        """Updates inspector according to the ResultView selected item"""
        self.entry = entry
        if not entry:
            self.btnRename.set_sensitive(False)
            self.btnDelete.set_sensitive(False)
            self.lblName.set_text('')
            self.lblMTime.set_text('')
            self.lblCTime.set_text('')
            self.lblATime.set_text('')
            self.lblPath.set_markup('')
            self.lblSize.set_text('')
            return
        
        self.btnRename.set_sensitive(True)
        self.btnDelete.set_sensitive(True)
        self.lblName.set_text(entry['baseName'])
        self._showSize()
        self.lblMTime.set_text(self._fmtTime(entry['mtime']))
        self.lblCTime.set_text(self._fmtTime(entry['ctime']))
        self.lblATime.set_text(self._fmtTime(entry['atime']))
        abbrevPath = entry['path'].replace(HOME_PATH, '~')
        lblPath = LBLPATH_FMT % (entry['path'], abbrevPath)
        self.lblPath.set_markup(lblPath)
