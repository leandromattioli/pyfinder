from urllib.request import urlparse
from urllib.parse import unquote

def uri2path(uri):
    return unquote(urlparse(uri).path)
