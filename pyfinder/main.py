#!/usr/bin/env python3

import sys
from pyfinder.app import PyFinderApp

def main():
    app = PyFinderApp()
    if app.config():
        app.run()
    else:
        app.activate()

if __name__ == '__main__':
    main()
