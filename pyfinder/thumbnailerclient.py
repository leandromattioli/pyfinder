#!/usr/bin/env python3
from os import path
from gi.repository import Gio, GnomeDesktop
from pyfinder.config import *

ICONS = {
    'folder': 'folder.png',
    #'text/x-csrc': 
    'generic': 'text-x-generic.png'
}

ICONS_FOLDER = '/usr/share/icons/Humanity/mimes/48'


class ThumbnailerClient:
    def __init__(self):
        self.factory = GnomeDesktop.DesktopThumbnailFactory.new(
            GnomeDesktop.DesktopThumbnailSize.LARGE
        )
        
    def _iconPath(self, icon):
        return path.join(DATA_PATH, 'icon_' + ICONS[icon])
        
    def _getIcon(self, fileName, mimeType):
        if mimeType in ICONS.keys():
            return self._iconPath(mimeType)
        fileName = mimeType.replace('/', '-') + '.svg'
        systemIcon = path.join(ICONS_FOLDER, fileName)
        if path.exists(systemIcon):
            return 'file://' + systemIcon
        return self._iconPath('generic')
        
    def makeThumbnail(self, fileName):
        if path.isdir(fileName):
            self._iconPath('folder')
        mtime = path.getmtime(fileName)
        # Use Gio to determine the URI and mime type
        f = Gio.file_new_for_path(fileName)
        uri = f.get_uri()
        info = f.query_info(
            'standard::content-type', 
            Gio.FileQueryInfoFlags.NONE, 
            None)
        mimeType = info.get_content_type()
        #Case 1: Updated thumb
        thumb = self.factory.lookup(uri, mtime)
        if thumb is not None:
            return thumb
        #Case 2: No thumbnail possible
        if not self.factory.can_thumbnail(uri, mimeType, mtime):
            return self._getIcon(fileName, mimeType)
        #Case 3: Thumbnail not in cache
        thumbPixBuf = self.factory.generate_thumbnail(uri, mimeType)
        if thumbPixBuf is None: #Error
            return self._getIcon(mimeType)
        self.factory.save_thumbnail(thumbPixBuf, uri, mtime)
        return self.factory.lookup(uri, mtime)

if __name__ == '__main__':
    import sys
    t = ThumbnailerClient()
    t.makeThumbnail(sys.argv[1])
