from math import floor
from gi.repository import Gtk, GdkPixbuf
from pyfinder.observable import Observable

#ListStore: basename, icon, filepath, order, id

ICON_WIDTH = 72
ITEM_WIDTH = 96

class IconResultView(Gtk.IconView, Observable):
    def __init__(self):
        Observable.__init__(self)
        self.store = Gtk.ListStore(str, GdkPixbuf.Pixbuf, str, int, int)
        #Reverse order already handled!
        self.store.set_sort_column_id(3, Gtk.SortType.ASCENDING)
        Gtk.IconView.__init__(self, self.store)
        self.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.set_text_column(0)
        self.set_pixbuf_column(1)
        self.set_item_width(ITEM_WIDTH)
        self.set_hexpand(False)
        self.connect('selection-changed', self.onSelectionChanged)
        self.connect("item-activated", self.onItemActivated)
        
    def onSelectionChanged(self, widget):
        selection = self.get_selected_items()
        if selection:
            item = self.store.get_iter(selection[0])
            idx = self.store[item][4]
            self.notifyAll('file-details', self.results[idx])
        else:
            self.notifyAll('selection-cleared')
        
    def onItemActivated(self, widget, item):
        model = widget.get_model()
        self.notifyAll('file-clicked', model[item][2])

    def view(self, results):
        self.store.clear()
        self.results = results
        for i in range(len(results)):
            e = results[i]
            basename = e['baseName']
            path = e['path']
            order = e['value']
            #Icon
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(e['thumb'])
            pix_w = pixbuf.get_width()
            pix_h = pixbuf.get_height()
            new_h = (pix_h * ICON_WIDTH) / pix_w 
            tiles = GdkPixbuf.InterpType.TILES
            pix = pixbuf.scale_simple(ICON_WIDTH, new_h, tiles)
            self.store.append([basename, pix, e['path'], e['value'], i])
