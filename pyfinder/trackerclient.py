from subprocess import Popen, PIPE
from functools import reduce
import shlex

class TrackerClient:
    def __init__(self):
        self._tags = []
        proc = Popen(['tracker-tag -t'], shell=True, stdout=PIPE)
        lines = proc.stdout.readlines()
        for l in lines:
            if not l.startswith(b'    ') and l.startswith(b'  '):
                self._tags.append(l.strip().decode('utf-8'))
        
    def getFileList(self, tags=None):
        #@note 'tracker-tag -s -t -l X' limits tags, not files!
        cmd = 'tracker-tag -s -t'
        #Case 1: All files
        if not tags:
            proc = Popen(cmd, shell=True, stdout=PIPE)
            lines = proc.stdout.readlines()
            f = [l.strip().decode('utf-8') for l in lines if b'file://' in l]
            return list(set(f))
        #Case 2: Tag filter
        cmd += ' %s'
        fileLists = []
        for t in tags:
            safeCmd = cmd % shlex.quote(t)
            proc = Popen(safeCmd, shell=True, stdout=PIPE)
            lines = proc.stdout.readlines()
            f = [l.strip().decode('utf-8') for l in lines if b'file://' in l]
            fileLists.append(f)
        #Finding intersection
        sets = [set(x) for x in fileLists]
        return list(reduce(lambda x,y : x & y, sets))
        
    def getAllTags(self):
        return self._tags   

if __name__ == "__main__":
    tracker = TrackerClient()
    print(tracker.getAllTags())
    print(tracker.getFileList())
