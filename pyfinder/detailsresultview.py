from math import floor
from time import gmtime, strftime
from gi.repository import Gtk, GdkPixbuf, Pango
from pyfinder.observable import Observable
from pyfinder.config import *

ICON_WIDTH = 24

class DetailsResultView(Gtk.TreeView, Observable):
    def __init__(self):
        Observable.__init__(self)
        self._store = Gtk.ListStore(
            str,              #basename (0)
            GdkPixbuf.Pixbuf, #icon     (1)
            str,              #filepath (2)
            int,              #order    (3)
            int,              #id       (4)
            str,              #abbreviated path (~ instead of HOME_PATH) (5)
            str,              #size (automatic unit) (6)
            str,              #access time            (7)
            str,              #modified time         (8)
            str,              #creation time         (9)
            int,              #size in bytes (sorting)                (10)
            int,              #atime in seconds since epoch (sorting) (11)
            int,              #mtime in seconds since epoch (sorting) (12)
            int)              #ctime in seconds since epoch (sorting) (13)
            
        #Reverse order already handled!
        Gtk.TreeView.__init__(self, self._store)
        self.set_hexpand(False)
        self.connect('cursor-changed', self.onCursorChanged)
        self.connect('row-activated', self.onRowActivated)
        self.set_rules_hint(True)
        self.set_headers_clickable(True)
        self.set_reorderable(True)
        self.set_enable_tree_lines(True)
        self._createColumns()
        
    def _helperColumn(self, label, colIndex, colOrderID):
        renderer = Gtk.CellRendererText()
        renderer.set_property("ellipsize_set", True)
        renderer.set_property("ellipsize", Pango.EllipsizeMode.END)
        column = Gtk.TreeViewColumn(label, renderer, text=colIndex)
        column.set_sort_column_id(colOrderID)  
        column.set_resizable(True) 
        column.set_min_width(50) 
        
        self.append_column(column)
        
    def _createColumns(self):
        #Icon
        rendererPixbuf = Gtk.CellRendererPixbuf()
        column = Gtk.TreeViewColumn(" ", rendererPixbuf, pixbuf=1)
        column.set_sort_column_id(1)
        column.set_resizable(False)
        self.append_column(column)
        #Other columns
        self._helperColumn("Name", 0, 0)
        self._helperColumn("Path", 5, 5)
        self._helperColumn("Size", 6, 10)
        self._helperColumn("Acessed", 7, 11)
        self._helperColumn("Modified", 8, 12)
        self._helperColumn("Created", 9, 13)
        
    def _fmtTime(self, epoch):
        """Formats time according to locale preferences"""
        #@TODO Get the formatting string from preferences
        t = gmtime(epoch)
        return strftime("%x %X", t)
        
                
    def onCursorChanged(self, widget):
        selection = self.get_selection()
        (model, rows) = selection.get_selected_rows()
        if rows:
            item = self._store.get_iter(rows[0])
            idx = self._store[item][4]
            self.notifyAll('file-details', self.results[idx])
        else:
            self.notifyAll('selection-cleared')
        
    def onRowActivated(self, widget, path, column):
        model = widget.get_model()
        item = model.get_iter(path)
        self.notifyAll('file-clicked', model[item][2])

    def view(self, results):
        self._store.clear()
        self.results = results
        for i in range(len(results)):
            e = results[i]
            
            #Icon
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(e['thumb'])
            pix_w = pixbuf.get_width()
            pix_h = pixbuf.get_height()
            new_h = (pix_h * ICON_WIDTH) / pix_w 
            tiles = GdkPixbuf.InterpType.TILES
            pix = pixbuf.scale_simple(ICON_WIDTH, new_h, tiles)
            
            #Abbreviated path
            abbrev = e['path'].replace(HOME_PATH, '~')
            
            #Size
            kilo = 2**10
            mega = 2**20
            giga = 2**30
            size = e['size']
            if size > giga:
                lblSize = '%.3f GB' % (size / giga)
            elif size > mega:
                lblSize = "%.3f MB" % (size / mega)
            elif size > kilo:
                lblSize = '%.3f KB' % (size / kilo)
            else:
                lblSize = '%.3f B' % size
            
            #Dates
            atime = self._fmtTime(e['atime'])
            mtime = self._fmtTime(e['mtime'])
            ctime = self._fmtTime(e['ctime'])
            
            #Columns
            columns = [
                e['baseName'], pix, e['path'], e['value'], i, abbrev,
                lblSize, atime, mtime, ctime, e['size'], e['atime'],
                e['mtime'], e['ctime'] ]
            self._store.append(columns)
