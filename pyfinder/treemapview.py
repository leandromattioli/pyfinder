from os import path
import json
from gi.repository import Gtk, WebKit
from pyfinder.config import *
from pyfinder.observable import Observable
from pyfinder.uriutils import uri2path

#@TODO: Refactor JavaScript and HTML to embedded resources

URL_TREEMAP = path.join(DATA_PATH, 'treemap.html')

JS_SHOW = '''
function highlightEntry(p) {
    $('p').removeClass('highlight');
    p.addClass('highlight');
}

$(document).ready(function () {
    var totalWidth = $(window).width() - 20;
    var totalHeight = $(window).height() - 20;
    if(!population || !population.length) {
        $('#treemap').html(' \
            <h2> \
                (Nenhum resultado encontrado) <br />  \
                <img src="notFound.png" alt="" /> \
            </h2> \
        ');
        return;
    }
    
    //Treemap
    $('#treemap').treemap({
	    data: population,
	    width: totalWidth,
	    height: totalHeight,
	    colors: ['#FFF', '#CCC']
    });
    var img = $('img');
    var div = img.closest('div');
    img.height(Math.floor(div.height() - 0.1 * div.height()));
    img.width(Math.floor(div.width() - 0.1 * div.width()));
    
    
    
    $('p').bind('click', function() {
        highlightEntry($(this));
        document.title = $(this).attr('id').split('_')[1];
        return false;
    });
    
    $('p a').bind('click', function() {
        var p = $(this).parent();
        highlightEntry(p);
        document.title = p.attr('id').split('_')[1];
        return false;
    });
    
    $('p').bind('dblclick', function() {
        highlightEntry($(this));
        var a = $(this).find('a')[0];
        window.location = a.href;
        return false;
    });
    
    $('p a').bind('dblclick', function() {
        highlightEntry($(this).parent());
        window.location = this.href;
        return false;
    });
});
'''

LABEL_FMT = '''
<p id="result_%s">
    <a href="%s">%s</a><br />
    <a href="%s"><img src="%s" alt="" /></a>
</p>
'''

class TreemapView(WebKit.WebView, Observable):
    def __init__(self):
        Observable.__init__(self)
        WebKit.WebView.__init__(self)
        self.connect('load-finished', self._loadFinished)
        self.connect('navigation-requested', self._clicked)
        self.connect('title-changed', self._titleChanged)
        self.set_size_request(640, 480)
        self.set_vexpand(True)
        self.treemapURI = 'file://' + path.abspath(URL_TREEMAP)
        #self.get_inspector().show()
        
    def _makeLabel(self, e, idx):
        params = (  idx,
                    e['uri'], 
                    e['baseName'], 
                    e['uri'], 
                    e['thumb'] )
        e['label'] = LABEL_FMT % params
        
    def view(self, results):
        self.results = results
        #Labels have indexes, list size is already determined
        for e in self.results:
            self._makeLabel(e, self.results.index(e))
        self.load_uri(self.treemapURI)
        
    # Events
        
    def _titleChanged(self, webview, frame, title, *data):
        if title.isnumeric():
            idx = int(title)
            e = self.results[idx]
            self.notifyAll('file-details', e)
        else:
            self.notifyAll('message', title)
    
    def _clicked(self, webview, frame, networkRequest):
        uri = networkRequest.get_uri()
        if uri != self.treemapURI:
            fileName = uri2path(uri)
            self.notifyAll('file-clicked', fileName)
            return WebKit.NavigationResponse.IGNORE
        return WebKit.NavigationResponse.ACCEPT
    
    def _loadFinished(self, widget, *data):
        contents = 'var population = %s;' % json.dumps(self.results)
        self.execute_script(contents)
        self.execute_script(JS_SHOW)
