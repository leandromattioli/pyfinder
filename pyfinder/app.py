from gi.repository import Gtk, Gio
from pyfinder.gui import GUI

class PyFinderApp(Gtk.Application):  
    def __init__(self):
        Gtk.Application.__init__(self,
                             application_id="com.leandromattioli.PyFinder", 
                             flags=Gio.ApplicationFlags.FLAGS_NONE)
                     
    def config(self):
        """Registers PyFinder application
        
        :returns: Whether the application is being launched for the 1st time
        """
        if not Gio.Application.register(self, None):
            print('Cannot register PyFinder application!')
            sys.exit(-1)
        if Gio.Application.get_is_remote(self):
            print('Activating remote application!')
            return False
        print('Creating GUI...')
        self.createGUI()
        return True
    
    def on_quit(self, *args):
        """Quit application"""
        self.quit()
    
    def createGUI(self):
        """Construct GUI"""
        self.gui = GUI()
        self.connect("activate", self.activateCb)
        self.add_window(self.gui.win)
        self.gui.win.connect('delete-event', self.on_quit)
        self.gui.show()
   
    def activateCb(self, app):
        """Shows application window"""
        self.gui.win.present()
