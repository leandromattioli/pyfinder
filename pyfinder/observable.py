class Observable:
    def __init__(self):
        self._observers = []
    
    def subscribe(self, obj):
        self._observers.append(obj)
        
    def unsubscribe(self, obj):
        if obj in self._observers:
            self._observers.remove(obj)
    
    def notifyAll(self, event, data=None):
        for o in self._observers:
            o.notify(self, event, data)
