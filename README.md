# README #

PyFinder is a GUI application that emulates a database filesystem on top of a regular filesystem. Currently it is integrated with Tracker for tagging files. You may think of it as an alternative for your file manager. Instead of organizing content with folders, you (also) organize with tags! This project is in early development stage. 

### Dependencies ###

* GTK
* WebKitGtk
* Keybinder
* Python 3
* Python GObject-Introspection
* Tracker Utilities

### Supported Platforms ###

* Linux

### How to install ###

This software package uses the standard Python distribution utility. Installing pyfinder is quite simple:


```
#!bash

python3 setup.py install
```

### Contribution guidelines ###

Soon!

### Who do I talk to? ###

Leandro Mattioli <leandro DOT mattioli AT gmail.com>